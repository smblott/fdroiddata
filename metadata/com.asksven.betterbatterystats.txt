AntiFeatures:UpstreamNonFree
Categories:System
# All source files and jars but no licence in README
License:Apache2
Web Site:http://asksven.tumblr.com
Source Code:https://github.com/asksven/BetterBatteryStats
Issue Tracker:https://github.com/asksven/BetterBatteryStats/issues

Auto Name:BetterBatteryStats
Summary:Monitor battery behaviour
Description:
Find applications causing the phone to drain battery while it is supposed to
be asleep and measure the effect of corrective actions:

* Spot drainers based on detailed information about the root cause
* Measure the effect of actions to reduce drain
* Detect changes in the awake/sleep profile and quickly find the causes (rogue apps)

The app displays bar graphs based on reference points as explained in the Getting
Started section of the Help menu and it is good idea to read that if you want to
have any chance of interpreting the information.

Requires root: On Android 4.4 it does. Otherwise you only need root
to view Network or Alarm stats.

Google Analytics and Locale SDK integration were removed.

[http://better.asksven.org/bbs-changelog Changelog]
.

Repo Type:git
Repo:https://github.com/asksven/BetterBatteryStats.git

# Need to patch out Locale because it causes build errors
Build:1.11.0.0,28
    commit=v1.11.0.0
    subdir=BetterBatteryStats
    srclibs=ActionBarSherlock@4.2.0,NoAnalytics@158a4a,Common-AskSven@7d71a6ba9a
    prebuild=sed -i 's@\(reference.1=\).*@\1$$Common-AskSven$$@' project.properties && \
        sed -i 's@\(reference.2=\).*@\1$$NoAnalytics$$@' project.properties && \
        sed -i 's@\(reference.3=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i '75,87d' AndroidManifest.xml && \
        rm -rf libs/libGoogleAnalytics.jar src/com/asksven/betterbatterystats/localeplugin *.apk

Build:1.13.4.0,38
    commit=ef3df22ba
    subdir=BetterBatteryStats
    srclibs=ActionBarSherlock@4.2.0,NoAnalytics@158a4a,Common-AskSven@b09bb8b6cd
    prebuild=sed -i 's@\(reference.1=\).*@\1$$Common-AskSven$$@' project.properties && \
        sed -i 's@\(reference.2=\).*@\1$$NoAnalytics$$@' project.properties && \
        sed -i 's@\(reference.3=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i '104,116d' AndroidManifest.xml && \
        rm -rf libs/libGoogleAnalytics.jar src/com/asksven/betterbatterystats/localeplugin

Build:1.14.0.0,39
    disable=missing part of Common-AskSven library v1.14.0.0
    commit=unknown - see disabled
    subdir=BetterBatteryStats
    srclibs=ActionBarSherlock@4.4.0,NoAnalytics@a63142b59,Common-AskSven@b09bb8b6cd
    prebuild=echo "android.library.reference.1=$$Common-AskSven$$" >> project.properties && \
        echo "android.library.reference.2=$$NoAnalytics$$" >> project.properties && \
        echo "android.library.reference.3=$$ActionBarSherlock$$" >> project.properties && \
        sed -i '106,140d' AndroidManifest.xml && \
        rm -rf libs/libGoogleAnalytics.jar     src/com/asksven/betterbatterystats/localeplugin

Build:1.15.0.0_B1,40
    disable=beta

Build:1.15.0.0_RC8,43
    commit=v1.15.0.0_RC8
    subdir=BetterBatteryStats
    rm=BetterBatteryStats/libs/libGoogleAnalytics.jar,BetterBatteryStats/src/com/asksven/betterbatterystats/localeplugin
    srclibs=RootTools@2.6,1:Common-AskSven@4d805,3:ActionBarSherlock@4.4.0,2:NoAnalytics@a63142b59
    prebuild=sed -i '115,127d' AndroidManifest.xml

Build:1.15.0.0,44
    commit=480c0d217ee
    subdir=BetterBatteryStats
    rm=BetterBatteryStats/libs/libGoogleAnalytics.jar,BetterBatteryStats/src/com/asksven/betterbatterystats/localeplugin
    srclibs=RootTools@2.6,1:Common-AskSven@4d805,3:ActionBarSherlock@4.4.0,2:NoAnalytics@d38ae535b543b515db
    prebuild=sed -i '115,127d' AndroidManifest.xml

Maintainer Notes:
Beware the stupid line number-based sed in prebuild. Check for Common-AskSven changes.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.15.0.0
Current Version Code:44

