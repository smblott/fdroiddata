Categories:Science & Education
License:GPLv3
Web Site:https://github.com/chaosbastler/opentraining
Source Code:https://github.com/chaosbastler/opentraining
Issue Tracker:https://github.com/chaosbastler/opentraining/issues

Auto Name:Open Training
Summary:Plan your fitness training
Description:
Create a fitness plan by targeting specific muscles and selecting from
the list.

When you've created a set you can export a spreadsheet to html for printing.
Some of the exercise names are in German but that shouldn't affect the app
usage.
.

Repo Type:git
Repo:https://github.com/chaosbastler/opentraining.git

Build:0.2.2,5
    commit=v0.2.2
    target=android-16
    prebuild=wget http://repo1.maven.org/maven2/junit/junit/4.10/junit-4.10.jar -P libs/

Build:0.2.3,6
    commit=v0.2.3
    target=android-16
    extlibs=junit/junit-4.10.jar

Build:0.3.1,8
    commit=v0.3.1
    subdir=app
    extlibs=junit/junit-4.10.jar
    srclibs=ActionBarSherlock@90939dc
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties

Build:0.4,11
    commit=v0.4
    subdir=app
    extlibs=junit/junit-4.10.jar
    srclibs=ActionBarSherlock@90939dc
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties

Build:0.5,18
    commit=v0.5
    subdir=app
    target=android-18
    extlibs=junit/junit-4.10.jar
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/

Build:0.5.1,19
    commit=v0.5.1
    subdir=app
    target=android-18
    extlibs=junit/junit-4.10.jar
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/

Build:0.6,22
    commit=v0.6
    subdir=app
    target=android-18
    extlibs=junit/junit-4.10.jar
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/

Build:0.6.2,25
    commit=v0.6.2
    subdir=app
    gradle=yes@..

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.6.2
Current Version Code:25

