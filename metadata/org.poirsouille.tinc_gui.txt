Categories:Internet
License:GPLv3+
Web Site:http://tinc_gui.poirsouille.org
Source Code:https://github.com/Vilbrekin/tinc_gui
Issue Tracker:https://github.com/Vilbrekin/tinc_gui/issues

Auto Name:Tinc
Summary:Port of Tinc VPN
Description:
Tinc GUI for Android is a (slightly modified) cross-compiled version of tincd,
associated with a basic GUI for daemon management.

Root is not needed, even if highly recommended for correct tinc daemon usage.
.

Repo Type:git
Repo:https://github.com/Vilbrekin/tinc_gui.git

Build:0.9.7-arm,8
    commit=ad35972299ad697a0770e7b8d9cb5b3778415a7f
    submodules=yes
    forceversion=yes
    rm=res/raw/tincd
    build=make -C src_tinc/ install

Maintainer Notes:
Uses armeabi binary in res/raw/, so no native-code appears and fdroid can't
filter by architecture. This is why -arm is appended, to at least let people
running x86 or MIPS know that it won't work for them yet.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.9.7
Current Version Code:8

