Categories:System
License:GPLv3
Web Site:http://sites.google.com/site/ghostcommander1
Source Code:http://sourceforge.net/p/gc-sftp/code
Issue Tracker:http://sourceforge.net/p/ghostcommander/_list/tickets
Donate:http://sourceforge.net/p/ghostcommander/donate

Auto Name:Ghost Commander - SFTP plugin
Summary:Access files over SFTP
Description:
A plug-in for [[com.ghostsq.commander]] to access secure FTP sites.
Launch Ghost Commander and go to 'Menu > Location > Home > SFTP site'.
Alternatively, scroll along the toolbar until you arrive at 'Home'.
Enter your server name and credentials.
.

# If you are building this yourself, config.py must be edited to sign this
# with the same key as Ghost Commander
Repo Type:git-svn
Repo:https://svn.code.sf.net/p/gc-sftp/code/trunk

Build:1.10b1,11
    commit=26
    extlibs=ganymed-ssh/ganymed-ssh2-build210.jar,custom_rules.xml
    srclibs=GhostCommander@390
    build=sed 's/jcifs-1.3.17/ganymed-ssh2-build210/' libs/custom_rules.xml > custom_rules.xml && \
        ant debug -f $$GhostCommander$$/build.xml && \
        jar c -C $$GhostCommander$$/bin/classes/ com > gc.jar && \
        install -D gc.jar libs/gc.jar

Build:1.11b1,12
    commit=28
    extlibs=ganymed-ssh/ganymed-ssh2-build210.jar,custom_rules.xml
    srclibs=GhostCommander@390
    build=sed 's/jcifs-1.3.17/ganymed-ssh2-build210/' libs/custom_rules.xml > custom_rules.xml && \
        ant debug -f $$GhostCommander$$/build.xml && \
        jar c -C $$GhostCommander$$/bin/classes/ com > gc.jar && \
        install -D gc.jar libs/gc.jar

Build:1.11b3,13
    disable=Doesn't compile
    commit=30

Maintainer Notes:
No commit comments whatsoever!

Few different forks of used SSH2 library exist, but https://code.google.com/p/ganymed-ssh-2
(which is in turn referenced by number of sites) claims, that it is the most genuine one
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.13
Current Version Code:16

