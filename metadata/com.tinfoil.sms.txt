Categories:Phone & SMS
License:GPLv3
Web Site:http://tinfoilhat.github.io/tinfoil-sms
Source Code:https://github.com/tinfoilhat/tinfoil-sms
Issue Tracker:https://github.com/tinfoilhat/tinfoil-sms/issues

Auto Name:Tinfoil-SMS
Summary:Encrypt text messages
Description:
Tinfoil-SMS encrypts your texts. It uses 256 bit ECC public keys as well
as a unique signed key exchange to prevent any "man-in-the-middle" attacks.
.

Repo Type:git
Repo:https://github.com/tinfoilhat/tinfoil-sms.git

Build:1.3.1,16
    disable=unverified jars
    commit=1.3.1-fdroid

Auto Update Mode:Version +-fdroid %v
Update Check Mode:Tags
Current Version:1.3.1
Current Version Code:16

