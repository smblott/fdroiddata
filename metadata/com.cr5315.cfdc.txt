Categories:Office
License:GPLv3
Web Site:http://cr5315.com
Source Code:https://github.com/cr5315/countdown-for-dashclock
Issue Tracker:https://github.com/cr5315/countdown-for-dashclock/issues

Auto Name:Countdown for DashClock
Summary:Countdown on the lock screen
Description:
Extension for [[net.nurik.roman.dashclock]] that displays the time
remaining until an event.
.

Repo Type:git
Repo:https://github.com/cr5315/countdown-for-dashclock.git

Build:1.3.2,17
    commit=f1dd7
    rm=libs/dashclock-api-r2.0.jar
    srclibs=DashClock@ecb5a191880
    prebuild=echo 'source.dir=src;$$DashClock$$/api/src/main/java;$$DashClock$$/api/src/main/aidl' >> project.properties

Build:1.3.3,18
    commit=2cf6d12841
    rm=libs/dashclock-api-r2.0.jar
    srclibs=DashClock@ecb5a191880
    prebuild=echo 'source.dir=src;$$DashClock$$/api/src/main/java;$$DashClock$$/api/src/main/aidl' >> project.properties

Maintainer Notes:
There are tags, but not up to date.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.3.4.1
Current Version Code:20

