Categories:Office
License:Apache2
Web Site:https://guardianproject.info
Source Code:https://github.com/guardianproject/notecipher
Issue Tracker:https://github.com/guardianproject/notecipher/issues

Auto Name:NoteCipher
Summary:Notepad with lock
Description:
Simple app for taking notes that encrypts everything behind a password.

Status: Beta.
.

Repo Type:git
Repo:https://github.com/guardianproject/notecipher.git

Build:0.1,12
    commit=8afa912172ce
    subdir=app
    srclibs=2:ActionBarSherlock@4a79d,3:NumberPicker-SimonVT@b4562,1:Cacheword@d49c128bb,IOCipher@v0.1
    prebuild=wget https://s3.amazonaws.com/sqlcipher/SQLCipher+for+Android+v2.2.2.zip && \
        rm $$Cacheword$$/libs/iocipher.jar && \
        $$SDK$$/tools/android update project -p $$Cacheword$$ -l ../../IOCipher
    build=unzip SQLCipher+for+Android+v2.2.2.zip && \
        cp -R SQLCipher\ for\ Android\ v2.2.2/libs/* libs/ && \
        cp -R SQLCipher\ for\ Android\ v2.2.2/assets/* assets/ && \
        rm libs/guava-r09.jar && \
        cp libs/android-support-v4.jar $$Cacheword$$/libs/ && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        cp libs/sqlcipher.jar $$Cacheword$$/libs/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:12

